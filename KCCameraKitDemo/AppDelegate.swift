//
//  AppDelegate.swift
//  KCCameraKitDemo
//
//  Created by Serhiy Vysotskiy on 6/7/17.
//  Copyright © 2017 Vysotskiy Serhiy. All rights reserved.
//

import Fabric
import Crashlytics
import UIKit
import KCCameraKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: CameraViewController.instantiate())
        window?.makeKeyAndVisible()
        
        Fabric.with([Crashlytics.self])
        return true
    }
    
    
}

